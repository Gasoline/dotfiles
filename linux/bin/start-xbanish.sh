#!/bin/bash

mkdir -p ~/.var/log/ 2> /dev/null
killall xbanish > /dev/null 2> /dev/null
nohup xbanish -t 5 > ~/.var/log/xbanish.log &
