#!/bin/bash

killall unclutter > /dev/null 2> /dev/null
unclutter --timeout 2 -b --start-hidden
